const mongoose = require('mongoose')


// Quiz Schema
const QuestionSchema = new mongoose.Schema({
  id: Number,
  name: String,
  description: String,
  question: [
    {
      id: Number,
      name: String,
      questionTypeId: Number,
      options: [
        {
          id: Number,
          name: String,
          isAnswer: Boolean
        },
        {
          id: Number,
          name: String,
          isAnswer: Boolean
        },
        {
          id: Number,
          name: String,
          isAnswer: Boolean
        }
      ],
      questionType: [
        {
          id: Number,
          name: String,
          isActive: Boolean
        }
      ]
    }
  ]
})

mongoose.model('Quiz',QuestionSchema)
